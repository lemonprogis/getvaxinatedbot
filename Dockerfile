FROM maven:3.8.1-openjdk-11 AS build
WORKDIR /usr/src/app

COPY pom.xml .
COPY src src
RUN mvn package

FROM maven:3.8.1-openjdk-11 AS unpack
WORKDIR /tmp/
COPY --from=build /usr/src/app/target/*.jar application.jar
RUN java -Djarmode=layertools -jar application.jar extract


FROM gcr.io/distroless/java:11
EXPOSE 8080
USER nonroot
COPY --from=unpack /tmp/* ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
