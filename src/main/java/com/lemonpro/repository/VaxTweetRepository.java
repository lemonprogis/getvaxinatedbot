package com.lemonpro.repository;

import com.lemonpro.model.VaxTweet;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class VaxTweetRepository {
    private final JdbcTemplate jdbcTemplate;

    public List<VaxTweet> getVaxTweets() {
        return jdbcTemplate.query("select id, tweet from VAX_TWEETS", (resultSet, i) ->
                VaxTweet.builder()
                        .id(resultSet.getInt("id"))
                        .tweet(resultSet.getString("tweet")).build());
    }
}
