package com.lemonpro.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VaxTweet {
    private Integer id;
    private String tweet;
}
