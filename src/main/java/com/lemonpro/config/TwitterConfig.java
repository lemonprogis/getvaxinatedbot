package com.lemonpro.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

@Configuration
public class TwitterConfig {
    @Bean
    public Twitter twitter(TwitterProperties twitterProperties) {
        TwitterFactory factory = new TwitterFactory();
        Twitter twitter = factory.getInstance();
        twitter.setOAuthConsumer(twitterProperties.getConsumerKey(), twitterProperties.getConsumerSecret());
        AccessToken accessToken = new AccessToken(twitterProperties.getAccessToken(), twitterProperties.getAccessTokenSecret());
        twitter.setOAuthAccessToken(accessToken);
        return twitter;
    }
}
