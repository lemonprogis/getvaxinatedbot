package com.lemonpro.route;

import com.lemonpro.config.TwitterProperties;
import com.lemonpro.service.TweetService;
import lombok.RequiredArgsConstructor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;
import twitter4j.Status;

@Component
@RequiredArgsConstructor
public class TwitterStreamingRouter extends RouteBuilder {
    private final TwitterProperties twitterProperties;

    @Override
    public void configure() throws Exception {
        from(buildTwitterUri())
                .streamCaching()
                .process( exchange -> {
                    Status status = exchange.getIn().getBody(Status.class);
                    exchange.getIn().setBody(status);
                })
                .bean(TweetService.class, "replyTo");
    }

    private String buildTwitterUri() {
        return String.format("twitter-timeline://mentions?user=%s&type=%s&delay=%s&filterOld=true" +
                        "&consumerKey=%s&consumerSecret=%s&accessToken=%s&accessTokenSecret=%s",
                twitterProperties.getUsername(),
                twitterProperties.getType(),
                twitterProperties.getPollingTime(),
                twitterProperties.getConsumerKey(),
                twitterProperties.getConsumerSecret(),
                twitterProperties.getAccessToken(),
                twitterProperties.getAccessTokenSecret());
    }
}
