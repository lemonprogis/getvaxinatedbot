package com.lemonpro.service;

import com.lemonpro.model.VaxTweet;
import com.lemonpro.repository.VaxTweetRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import twitter4j.*;

import java.util.List;
import java.util.Random;

@Component
@RequiredArgsConstructor
@Slf4j
public class TweetService {
    private final Twitter twitter;
    private final VaxTweetRepository vaxTweetRepository;

    public void showTweet(Status status) {
        User user = status.getUser();
        String text = status.getText();
        String screenName = user.getScreenName();
        log.info("{} | {}", text, screenName);
    }

    public void replyTo(Status status) throws TwitterException {
        StatusUpdate statusUpdate = new StatusUpdate(getRandomVaxTweet());
        statusUpdate.inReplyToStatusId(status.getInReplyToStatusId());
        twitter.updateStatus(statusUpdate);
    }

    private String getRandomVaxTweet() {
        Random random = new Random();
        List<VaxTweet> vaxTweets = vaxTweetRepository.getVaxTweets();
        return vaxTweets.get(random.nextInt(vaxTweets.size())).getTweet();
    }
}
